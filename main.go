package memcached

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/textproto"
	"time"
)

const (
	DefaultKeepAlivePeriod     = time.Second * 10
	ReadWriteTimeout           = time.Millisecond * 500
	DefaultReadBuffer      int = 512
)

var (
	resultOK        = []byte("OK")
	resultStored    = []byte("STORED")
	resultNotStored = []byte("NOT_STORED")
	resultExists    = []byte("EXISTS")
	resultNotFound  = []byte("NOT_FOUND")
	resultDeleted   = []byte("DELETED")
	resultEnd       = []byte("END")
	resultOk        = []byte("OK")
	resultTouched   = []byte("TOUCHED")
)

type Memcached struct {
	conn net.Conn
}

func main() {}

func NewConn(addr string, timeout time.Duration) (*Memcached, error) {
	rAddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, err
	}

	conn, err := net.DialTCP("tcp", nil, rAddr)
	if err != nil {
		return nil, err
	}

	err = conn.SetKeepAlive(true)
	if err != nil {
		return nil, err
	}

	err = conn.SetKeepAlivePeriod(DefaultKeepAlivePeriod)
	if err != nil {
		return nil, err
	}

	err = conn.SetReadBuffer(DefaultReadBuffer)
	if err != nil {
		return nil, err
	}

	memc := &Memcached{
		conn: conn,
	}

	return memc, nil
}

func (m Memcached) Set(key string, expiration time.Duration, value []byte) error {
	return m.addValue("set", key, expiration, value)
}

func (m Memcached) Add(key string, expiration time.Duration, value []byte) error {
	return m.addValue("add", key, expiration, value)
}

func (m Memcached) request(reqCmd string) error {
	_, err := m.conn.Write([]byte(reqCmd))
	if err != nil {
		return err
	}

	err = m.conn.SetReadDeadline(time.Now().Add(ReadWriteTimeout))
	if err != nil {
		return err
	}
	return nil
}

func (m Memcached) addValue(cmd, key string, expiration time.Duration, value []byte) error {
	var (
		reqCmd string
		err    error
	)
	reqCmd = fmt.Sprintf(
		"%s %s %d %0.f %d\r\n%s\r\n",
		cmd,
		key,
		0x01,
		expiration.Seconds(),
		len(value),
		string(value),
	)

	err = m.request(reqCmd)
	if err != nil {
		return err
	}

	var buf bytes.Buffer
	var line []byte

	reader := bufio.NewReader(m.conn)
	tp := textproto.NewReader(reader)

	line, err = tp.ReadLineBytes()
	if err != nil {
		return err
	}

	buf.Write(line)

	switch {
	case bytes.Equal(line, resultStored):
		return nil
	case bytes.Equal(line, resultNotStored), bytes.Equal(line, resultExists), bytes.Equal(line, resultNotFound):
		return errors.New(string(line))
	}

	return fmt.Errorf("Unexpected response: %s", string(line))
}

func (m Memcached) Get(key string) ([]byte, error) {
	var (
		reqCmd string
		err    error
	)
	reqCmd = fmt.Sprintf(
		"get %s\r\n",
		key,
	)

	err = m.request(reqCmd)
	if err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	var line []byte

	reader := bufio.NewReader(m.conn)
	tp := textproto.NewReader(reader)

	var lineValueCmd []byte = []byte(fmt.Sprintf("VALUE %s ", key))
	for {
		line, err = tp.ReadLineBytes()
		if err != nil {
			return nil, err
		}

		switch {
		case bytes.Contains(line, lineValueCmd):
			continue
		case bytes.Equal(line, resultEnd):
			if buf.Len() < 2 {
				return nil, errors.New(string(resultNotStored))
			}
			return buf.Bytes()[2:], nil
		default:
			buf.Write([]byte{0x0D, 0x0A})
			buf.Write(line)
		}
	}

	return nil, fmt.Errorf("Unexpected response: %s", string(line))
}

func (m Memcached) Delete(key string) error {
	var (
		reqCmd string
		err    error
	)
	reqCmd = fmt.Sprintf(
		"delete %s\r\n",
		key,
	)

	err = m.request(reqCmd)
	if err != nil {
		return err
	}

	var line []byte

	reader := bufio.NewReader(m.conn)
	tp := textproto.NewReader(reader)

	for {
		line, err = tp.ReadLineBytes()
		if err != nil {
			return err
		}

		switch {
		case bytes.Equal(line, resultDeleted):
			return nil
		case bytes.Equal(line, resultNotFound):
			return errors.New(string(line))
		}
	}

	return fmt.Errorf("Unexpected response: %s", string(line))
}
